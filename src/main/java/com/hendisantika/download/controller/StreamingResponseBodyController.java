package com.hendisantika.download.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-download-file-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/09/18
 * Time: 16.35
 * To change this template use File | Settings | File Templates.
 */

@RestController
@Slf4j
public class StreamingResponseBodyController {
    String fileName = "resources/spring-boot-reference.pdf";

    @GetMapping("downloadFile")
    public StreamingResponseBody getSteamingFile(HttpServletResponse response) throws IOException {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=\"spring-boot-reference.pdf\"");
        InputStream inputStream = new FileInputStream(new File("/tmp/spring-boot-reference.pdf"));
        return outputStream -> {
            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                log.info("Writing some bytes .....");
                outputStream.write(data, 0, nRead);
            }
        };
    }

    @GetMapping("downloadFile1")
    public void getSteamingFile1(HttpServletResponse response) throws IOException {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=\"spring-boot-reference.pdf\"");
        InputStream inputStream = new FileInputStream(new File("/tmp/spring-boot-reference.pdf"));
        int nRead;
        while ((nRead = inputStream.read()) != -1) {
            response.getWriter().write(nRead);
        }
    }

    @GetMapping("downloadFile2")
    public InputStreamResource FileSystemResource(HttpServletResponse response) throws IOException {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=\"spring-boot-reference.pdf\"");
        InputStreamResource resource = new InputStreamResource(new FileInputStream("/tmp/spring-boot-reference.pdf"));
        return resource;
    }
}
