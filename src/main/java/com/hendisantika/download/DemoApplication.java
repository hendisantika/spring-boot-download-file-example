package com.hendisantika.download;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
        return "Spring is here! " + new Date();
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}